
/**
 * Buble sort implementation
 *
 * @author  (Liu Bin)
 * @version (0.01)
 */
public class BubbleSort implements SortInterface
{

    /**
     * Constructor for objects of class BubbleSort
     */
    public BubbleSort()
    {

    }

    public int sort(int[] arr)
    {
        int retval = 0;
        int n = arr.length;  
        int temp = 0;  
        
        //System.out.println("I am doing bubble sort...");  
        for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                         if(arr[j-1] > arr[j]){  
                                 //swap elements  
                                 temp = arr[j-1];  
                                 arr[j-1] = arr[j];  
                                 arr[j] = temp;  
                         }  
                          
                 }  
        }  
        
        return retval;
    }
    
    
    public String name()
    {
        return "Bubble Sort";
    }
    
}
