
/**
 * Quick sort implementation
 *
 * @author  (Liu Bin)
 * @version (0.01)
 */
public class QuickSort implements SortInterface
{

    /**
     * Constructor for objects of class BubbleSort
     */
    public QuickSort()
    {

    }
    
    private void exchange(int[] arr, int i, int j) 
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    
    private void doSort(int[] arr, int low, int high) 
    {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = arr[low + (high-low)/2];

        // Divide into two lists
        while (i <= j) {
            // If the current value from the left list is smaller than the pivot
            // element then get the next element from the left list
            while (arr[i] < pivot) {
                i++;
            }
            // If the current value from the right list is larger than the pivot
            // element then get the next element from the right list
            while (arr[j] > pivot) {
                j--;
            }

            // If we have found a value in the left list which is larger than
            // the pivot element and if we have found a value in the right list
            // which is smaller than the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(arr, i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            doSort(arr, low, j);
        if (i < high)
            doSort(arr, i, high);
    }
    
    public int sort(int[] arr)
    {
        int retval = -1;
        
        // check for empty or null array
        if (arr ==null || arr.length==0){
            return retval;
        }
        
        retval = 0;
        doSort(arr, 0, arr.length - 1);
        
        return retval;
    }
    
    
    public String name()
    {
        return "Quick Sort";
    }
    
}
