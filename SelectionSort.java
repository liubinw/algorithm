
/**
 * Selection sort implementation
 *
 * @author (Liu Bin)
 * @version (0.01)
 */
public class SelectionSort implements SortInterface
{

    /**
     * Constructor for objects of class SelectionSort
     */
    public SelectionSort()
    {

    }

    
    public int sort(int[] arr)
    {
        int retval = 0;
        int n = arr.length; 
        
        //System.out.println("I am doing selection sort...");  
        for (int i = 0; i < n - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < n; j++)
                if (arr[j] < arr[index]) 
                    index = j;
      
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }    
        
        return retval;
    }
    
    
    public String name()
    {
        return "Selection Sort";
    }
    

}
