
/**
 * interface for number sorting.
 * 
 * @author (Liu Bin)
 * @version (0.01)
 */
public interface SortInterface
{
    /**
     * To sort number from low to high(small to big)
     *
     * @param  arr is an array of un-sorted integer
     * @return arr is now sorted, the return value shows error status
     *       0: no error
     *       -1: error
     */
    public int sort(int[] arr);
    
    
    public String name();
    
}
