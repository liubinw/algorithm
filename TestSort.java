
/**
 * class TestSort is the main function for testing different sort algorthm
 *
 * @author  (Liu Bin)
 * @version (0.01)
 */
import java.util.Scanner;  
public class TestSort
{
    public static final int MAX_NUMBER_PRINT = 16;
    
    /**
     * Constructor for objects of class TestSort
     */
    public TestSort()
    {

    }

    static SortInterface chooseSort()
    {
        int a;
        Scanner in = new Scanner(System.in); 
        
        SortInterface quick = new QuickSort();
        SortInterface selection = new SelectionSort();
        SortInterface buble = new BubbleSort();   

        System.out.println("Please choose your sort algorithm:");
        System.out.println(" 1. " + quick.name());
        System.out.println(" 2. " + selection.name());
        System.out.println(" 3. " + buble.name());
        
        System.out.println("Enter your chose(a integer number):");  
        a = in.nextInt(); 
        if(a==1)
        {
          System.out.println("You choose: 1," + quick.name());
          return quick;
        }
        else if(a==2)
        {
           System.out.println("You choose: 2," + selection.name());
           return selection;           
        }
        else if(a==3)
        {
          System.out.println("You choose: 3," + buble.name());
          return buble;            
        }
        else
        {
          System.out.println("You enter: " + a + ", which is not a valid chose");
        }
        
        return null;
    }
    
    /**
     * run this method to start testing sorting
     *
     * @param     none
     * @return    none
     */
    public static void main()
    {
        long startTime, estimatedTime;
        int numberPrint;
        
        SortInterface testSort = chooseSort();
        if(testSort==null) return;
        
        // generate an array of integers
        //int sarr[] ={3,60,35,2,45,320,5};  
        int[] sarr = rand_number.gen_rand();
        
        // display the array before sorting
        if(sarr.length < MAX_NUMBER_PRINT) 
            numberPrint = sarr.length;
        else
            numberPrint = MAX_NUMBER_PRINT;
            
         System.out.println("Array Before Sort");  
         for(int i=0; i < numberPrint; i++){  
             System.out.print(sarr[i] + " ");  
         }  
           
         System.out.println();  
         System.out.println(); 
         
         Solver.draw_1d_array(sarr);
         
         // record the start time to compare with time after sorting
         startTime = System.nanoTime();

         // do the sorting
         System.out.println("I am doing " + testSort.name() + ", sorting number:" + sarr.length + "\n"); 
         testSort.sort(sarr); //sorting array elements using selection sort  
         
         // get the end time and print out the time duration used for sorting
         estimatedTime = System.nanoTime() - startTime;
         System.out.println("Array After Sort [elapse time:" + estimatedTime + " nano seconds] " );  

         // display array after sorting
         for(int i=0; i < numberPrint; i++){  
                System.out.print(sarr[i] + " ");  
         }          
         
         Solver.draw_1d_array(sarr);
        

    }
}
