import java.util.Scanner;  
/**
 * Write a description of class input here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class input
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class input
     */
    public input()
    {
        // initialise instance variables
        x = 0;
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    public static int sampleMethod(int y)
    {
        Scanner in = new Scanner(System.in); 
        int a;  
        float b;  
        String s;          
        // put your code here
        
        System.out.println("Enter a string");
        s = in.nextLine();  
        System.out.println("You entered string "+s);
        
        System.out.println("Enter an integer");  
        a = in.nextInt(); 
        System.out.println("You entered integer "+a);  
        System.out.println("Enter a float");  
        b = in.nextFloat(); 
        System.out.println("You entered float "+b);  
        
        return y;
    }
}
