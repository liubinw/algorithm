import java.util.Random;

/**
 * Write a description of class rand_number here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class rand_number
{
    // instance variables - replace the example below with your own
    private static int m_size = 2000;

    /**
     * Constructor for objects of class rand_number
     */
    public rand_number()
    {
        // initialise instance variables
        //size = 100;
    }

    /**
     * generate random numbers
     *
     * @param  none
     * @return    none
     */
    public static int[] gen_rand()
    {
        Random r = new Random();
        int[] ran_num = new int[m_size];
        for(int i=0;i<m_size;++i)
        {
            ran_num[i] = r.nextInt()%(10*m_size)/20;
            //System.out.println("i["+i+"]:"+ran_num[i]);
        }
        
        return ran_num;
    }
    
    public static void print_rand()
    {
        int[] ran_array = gen_rand();
        
        for(int i=0;i<m_size;++i)
        {
            System.out.println("i["+i+"]:"+ran_array[i]);
        }
                
    }
}
