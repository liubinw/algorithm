
/**
 * Write a description of class sort1 here.
 *
 * @author (your name)
 * @version (a version number or a date)
 * 
 * 
 * 
 */
public class sort1
{
    // instance variables - replace the example below with your own
    private int x;

    /**
     * Constructor for objects of class sort1
     */
    public sort1()
    {
        // initialise instance variables
        x = 0;
        
        System.out.println("Testing sort:");
    }

    private static void selectionSort(int[] arr){
        int n = arr.length; 
        System.out.println("I am doing selection sort...");  
        for (int i = 0; i < n - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < n; j++)
                if (arr[j] < arr[index]) 
                    index = j;
      
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
          
    }
     
    private static void bubbleSort(int[] arr) {  
        int n = arr.length;  
        int temp = 0;  
        
        System.out.println("I am doing bubble sort...");  
        for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                         if(arr[j-1] > arr[j]){  
                                 //swap elements  
                                 temp = arr[j-1];  
                                 arr[j-1] = arr[j];  
                                 arr[j] = temp;  
                         }  
                          
                 }  
        }  
  
    }  
    
    /**
     * run this method to start sorting
     *
     * @param  none
     * @return    none
     */
    public static void main()
    {
        long startTime, estimatedTime;
        // code starts from here
         int sarr[] ={3,60,35,2,45,320,5};  
         //int[] sarr = rand_number.gen_rand();
         
         System.out.println("Array Before Sort");  
         for(int i=0; i < sarr.length; i++){  
             System.out.print(sarr[i] + " ");  
         }  
         
         
         
         
         System.out.println();  
         
         startTime = System.nanoTime();
         //bubbleSort(sarr);//sorting array elements using bubble sort  
         
         selectionSort(sarr); //sorting array elements using selection sort  
         
         estimatedTime = System.nanoTime() - startTime;
    

         System.out.println("Array After Sort [elapse time:" + estimatedTime + "]" );  

         for(int i=0; i < sarr.length; i++){  
                System.out.print(sarr[i] + " ");  
         }          
 
        

    }
}
